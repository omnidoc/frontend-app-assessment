# <img src="resources/omnidoc-logo.png" alt="OmniDoc Logo" width="120px"/> Developer assessment 

![OmniDoc Logo](resources/logo.jpeg)

## What are we looking for?

We are looking for members who are interested in being part of an agile, progressive, proactive team with new ideas.
We seek to use technology as an ally to reach our goals as well as to offer customers a better user experience as well as delivering added value in our products.

## Assessment

Using the next API Doc https://any-api.com/spotify_com/spotify_com/docs/API_Description create an app with the follow features:

NOTE: API Spotify References https://developer.spotify.com/documentation/web-api/reference/

- Show the albums list for an artist given
- Show the tracks list for an album given
- Show tracks detail

NOTES:

- Feel free to design the UI for this app, show the fields that you consider important
- Authorization guide https://developer.spotify.com/documentation/general/guides/authorization-guide

### Requirement

- Add file README.md with instructions to run the project
- IOS Developer (Swift)
- Android Developer (Kotlin)
- Flutter Developer (Apps)
- Web Developer (React)
- You can use local storage solution if you required

### Evaluation

- We´re going to evaluate best practices, standards, logic, any effort to write clean code, maintainable, readeable etc.

Enjoy Coding!
